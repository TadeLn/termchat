# TermChat

TermChat is a client-server chat program. It is meant to be an alternative to IRC with additional functionality.

TermChat is currently under heavy development.

## Usage

1. Clone the Git repository
```
git clone --recursive https://gitlab.com/TadeLn/termchat.git
```
2. Install nessesary libraries

### Debian (and derivatives, ex. Ubuntu)

```
sudo apt install libsfml-dev libncurses-dev libnotify-dev
```

### Windows

TODO

### macOS

```
brew install sfml ncurses
```

### Other OSes

Click [here](https://www.google.com/) or [here](https://duckduckgo.com/).

3. Compile server and client
```
make
```
4. Run the server
```
server/server
```
5. Connect to it with a client
```
client/client --ip localhost
```

## Client commands

`/nick <nick>` - Changes your nickname to `<nick>`.

`/me <status>` - Updates your status as `* nick <status>`.

`/exit` - Disconnect and quit. The same as `Ctrl-C`.

When the first character is a dot (`.`), it is ignored, but the rest of the message is literal (commands are ignored).

## Configuration

### Client

Currently client configuration is worked on. See issue #7.

### Server

`listener.ip` (string) - IP to listen on. Defaults to `0.0.0.0`.

`listener.port` (int) - Port to listen on. Defaults to `16310`.

`logging.toConsole` (bool) - Enables printing events to the console. Defaults to `true`.

`logging.toFile` (bool) - Enables printing events to a file. Defaults to `false`.

`logging.file` (bool) - Specifies file name to log to. Defaults to `termchat.log`.

## Protocol

Documented in [the wiki](https://gitlab.com/TadeLn/termchat/-/wikis/home). Is considered public domain and can be used for anything you want.

## Features

- Client and server programs
- Nicknames
- Sending messages
- Status updates via `/me` command
- Recieving messages and other packets
- UI in `ncurses`
- Windows and Linux builds

## Roadmap

- [x] ncurses UI
- [ ] Reworking server core (see issue #1)
- [ ] Separating core functionality from UI and moving it to a C++ library (licensed under a permissive license, see issue #3)
- [ ] Client-server encryption (via SSL, see issue #2)