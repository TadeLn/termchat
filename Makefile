.PHONY: client server

all: client server

release: client_release client_release_windows_64 client_release_windows_32 server_release server_release_windows_64 server_release_windows_32

client:
	g++ client/main.cpp \
	-Ilib/CLI11/include \
	-Ilib/json/include \
	-o client/client \
	-lsfml-network -lsfml-system -lsfml-window -lsfml-audio -lncursesw -lpthread \
	`pkg-config --cflags --libs libnotify` \
	-D FEATURE_NOTIFICATION_TOAST \
	-D FEATURE_NOTIFICATION_SOUND

client_debug:
	g++ -g \
	client/main.cpp \
	-Ilib/CLI11/include \
	-Ilib/json/include \
	-o client/client \
	-lsfml-network -lsfml-system -lsfml-audio -lncursesw -lpthread \
	`pkg-config --cflags --libs libnotify` \
	-D DEBUG_SHOW_PACKETS \
	-D FEATURE_NOTIFICATION_TOAST \
	-D FEATURE_NOTIFICATION_SOUND

client_release:
	g++ -O3 \
	client/main.cpp \
	-Ilib/CLI11/include \
	-Ilib/json/include \
	-o client/client \
	-lsfml-network -lsfml-system -lsfml-audio -lncursesw -lpthread \
	`pkg-config --cflags --libs libnotify` \
	-D FEATURE_NOTIFICATION_TOAST \
	-D FEATURE_NOTIFICATION_SOUND

client_release_windows_64:
	x86_64-w64-mingw32-g++-posix -O3 \
	client/main.cpp \
	-Ilib/CLI11/include \
	-Ilib/json/include \
	-Ilib/windows/ncurses_64/include \
	-Llib/windows/ncurses_64/lib \
	-Ilib/windows/sfml_64/include -Llib/windows/sfml_64/lib \
	-o client/client.exe \
	-Wl,-Bstatic -static-libstdc++ -static-libgcc \
	-Wl,-Bdynamic -lncursesw -lsfml-network -lsfml-system -lsfml-audio \
	-D FEATURE_NOTIFICATION_SOUND

client_release_windows_32:
	i686-w64-mingw32-g++-posix -O3 \
	client/main.cpp \
	-Ilib/CLI11/include \
	-Ilib/json/include \
	-Ilib/windows/ncurses_32/include \
	-Llib/windows/ncurses_32/lib \
	-Ilib/windows/sfml_32/include -Llib/windows/sfml_32/lib \
	-o client/client32.exe \
	-Wl,-Bstatic -static-libstdc++ -static-libgcc \
	-Wl,-Bdynamic -lncursesw -lsfml-network -lsfml-system -lsfml-audio \
	-D FEATURE_NOTIFICATION_SOUND

server:
	g++ server/main.cpp \
	-Ilib/CLI11/include -Ilib/json/include \
	-o server/server \
	-lsfml-network -lsfml-system

server_debug:
	g++ -g \
	server/main.cpp \
	-Ilib/CLI11/include -Ilib/json/include \
	-o server/server \
	-lsfml-network -lsfml-system

server_release:
	g++ -O3 \
	server/main.cpp \
	-Ilib/CLI11/include \
	-Ilib/json/include \
	-o server/server \
	-lsfml-network -lsfml-system

server_release_windows_64:
	x86_64-w64-mingw32-g++-posix -O3 \
	server/main.cpp \
	-Ilib/CLI11/include \
	-Ilib/json/include \
	-Ilib/windows/sfml_64/include -Llib/windows/sfml_64/lib \
	-o server/server.exe \
	-Wl,-Bstatic -static-libstdc++ -static-libgcc \
	-Wl,-Bdynamic -lsfml-network -lsfml-system

server_release_windows_32:
	i686-w64-mingw32-g++-posix -O3 \
	server/main.cpp \
	-Ilib/CLI11/include \
	-Ilib/json/include \
	-Ilib/windows/sfml_32/include -Llib/windows/sfml_32/lib \
	-o server/server32.exe \
	-Wl,-Bstatic -static-libstdc++ -static-libgcc \
	-Wl,-Bdynamic -lsfml-network -lsfml-system

