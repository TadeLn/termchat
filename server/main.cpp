#define CLIENT_SLEEP_TIME 100
#define NEW_CONNECTION_SLEEP_TIME 1000

#include <iostream>
#include <string>
#include <thread>
#include <algorithm>
#include <csignal>

#include <CLI/CLI.hpp>
#include <nlohmann/json.hpp>
#include <SFML/Network.hpp>

nlohmann::json config;

std::stringstream makeSS() {
    std::stringstream ss;
    ss << '\n';
    return ss;
}

template<typename First, typename ... Rest>
std::stringstream makeSS(First first, Rest... rest) {
    std::stringstream ss;
    ss << first << makeSS(rest...).str();
    return ss;
}

template<typename ... Args>
void log(Args... args) {
    std::stringstream ss = makeSS(args...);

    bool logConsole = 1, logFile = 0;
    if (!config["logging"]["toConsole"].is_null()) {
        logConsole = config["logging"]["toConsole"];
    }
    if (!config["logging"]["toFile"].is_null()) {
        logFile = config["logging"]["toFile"];
    }

    if (logConsole) {
        std::cout << ss.str();
    }
    if (logFile) {
        std::string filename = "termchat.log";
        if (!config["logging"]["toFile"].is_null()) {
            filename = config["logging"]["file"];
        }
        
        std::ofstream file(filename, std::ios::app);
        file << ss.str();
    }
}



enum MessageType {
    DATA_NULL,
    DATA_TEXT,
    DATA_ME,
    DATA_CHANGENICK,
    DATA_NICKDATA,
    DATA_CONNECTED,
    DATA_DISCONNECTED
};

class Packet {

private:
    std::vector<unsigned char> bytes;
    void newPacket(unsigned int);
    void setType(unsigned char);
    void setUserId(unsigned int);

public:
    std::vector<unsigned char> getBytes();
    void newTextPacket(unsigned int, std::string);
    void newMePacket(unsigned int, std::string);
    void newNickPacket(unsigned int, std::string);
    void newNickDataPacket(unsigned int, std::string);
    void newConnectedPacket(unsigned int);
    void newDisconnectedPacket(unsigned int);

};

void Packet::newPacket(unsigned int size) {
    bytes.clear();
    bytes.resize(6, 0);
}


void Packet::setType(unsigned char type) {
    bytes[0] = type;
}

void Packet::setUserId(unsigned int id) {
    bytes[1] = id % 256;
    bytes[2] = id >> 8 % 256;
    bytes[3] = id >> 16 % 256;
    bytes[4] = id >> 24 % 256;
}

std::vector<unsigned char> Packet::getBytes() {
    return bytes;
}

void Packet::newTextPacket(unsigned int id, std::string message) {
    newPacket(5 + message.length());
    setType(DATA_TEXT);
    setUserId(id);
    std::copy(message.begin(), message.end(), bytes.begin() + 5);
};

void Packet::newMePacket(unsigned int id, std::string message) {
    newPacket(5 + message.length());
    setType(DATA_ME);
    setUserId(id);
    std::copy(message.begin(), message.end(), bytes.begin() + 5);
};

void Packet::newNickPacket(unsigned int id, std::string message) {
    newPacket(5 + message.length());
    setType(DATA_CHANGENICK);
    setUserId(id);
    std::copy(message.begin(), message.end(), bytes.begin() + 5);
};

void Packet::newNickDataPacket(unsigned int id, std::string message) {
    bytes.clear();
    bytes.resize(5 + message.length(), 0);
    setType(DATA_NICKDATA);
    setUserId(id);
    std::copy(message.begin(), message.end(), bytes.begin() + 5);
};

void Packet::newConnectedPacket(unsigned int id) {
    newPacket(6);
    setType(DATA_CONNECTED);
    setUserId(id);
};

void Packet::newDisconnectedPacket(unsigned int id) {
    newPacket(6);
    setType(DATA_DISCONNECTED);
    setUserId(id);
};



bool stop = false;
void stopServer(int signum) {
    log("Interrupt signal (", signum, ") received.\n");
    stop = true;
}



class Client;

class ClientConnection {

private:
    bool connected;
    Client* c;

public:
    Client* getClient();
    void connect(Client*);
    void disconnect();
    bool isConnected();

    ClientConnection();
    ClientConnection(Client*);
    ~ClientConnection();

};

class Client {

private:
    sf::TcpSocket *socket;
    std::thread t_listen;

public:
    std::vector<ClientConnection> *allConnections;
    int id;
    std::string nick;

    static void listen(Client*);
    void sendBytes(std::vector<unsigned char>);

    void startListening();

    Client(std::vector<ClientConnection>*, unsigned int);
    ~Client();

};

class Server {
    sf::TcpListener listener;

public:
    std::vector<ClientConnection> connections;
    void sendToAllClients(Packet);
    void sendToClient(unsigned int, Packet);
    void listen();
    Server(unsigned int, sf::IpAddress);

};

void Server::sendToAllClients(Packet packet) {
    for (unsigned int i = 0; i < connections.size(); i++) {
        sendToClient(i, packet);
    }
}

void Server::sendToClient(unsigned int clientId, Packet packet) {
    connections[clientId].getClient()->sendBytes(packet.getBytes());
}

void Server::listen() {
    while (!stop) {
        std::this_thread::sleep_for(std::chrono::milliseconds(NEW_CONNECTION_SLEEP_TIME));

        sf::TcpSocket *newSocket = new sf::TcpSocket();

        bool connectionError = listener.accept(*newSocket) != sf::Socket::Done;
        log("Creating a new connection...");
        if (connectionError) {
            log("Connection Error!");
            continue;
        }

        Client *newClient = new Client(&connections, connections.size());
        ClientConnection newConnection(newClient);
        connections.push_back(newConnection);
        log("Connection #", newClient->id, " established!");

        newClient->startListening();
    }
}

Server::Server(unsigned int port, sf::IpAddress ip) {
    log("Creating a Listener...");
    // Bind the listener to a port
    if (listener.listen(port, ip) != sf::Socket::Done)
    {
        log("Listener Error!");
    }
    log("Creating a Listener... Done!");
}



void Client::listen(Client *c) {
    while (c->allConnections->at(c->id).isConnected()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(CLIENT_SLEEP_TIME));

        log("listening#", c->id, "\n");
    }
}

void Client::sendBytes(std::vector<unsigned char> byteVector) {
    char *byteArray = new char[byteVector.size()];

    // Copy vector to array
    std::copy(byteVector.begin(), byteVector.end(), byteArray);

    // Send the bytes
    if (socket->send(byteArray, byteVector.size()) != sf::Socket::Done) {
        // Socket Error
        log("Socket ", id, " Error!\n");

        // Delete the array
        delete byteArray;

        // Close the connection, delete the client
        allConnections->at(id).disconnect();
    }

    // Delete the array
    delete byteArray;
}

void Client::startListening() {
    t_listen = std::thread(Client::listen, this);
}

Client::Client(std::vector<ClientConnection>* allConnections, unsigned int id) {
    this->allConnections = allConnections;
    this->id = id;
    nick = "";
}

Client::~Client() {
    t_listen.join();
}



Client* ClientConnection::getClient() { return c; }

void ClientConnection::connect(Client* ptr) {
    log("Creating a new connection...");
    connected = true;
    c = ptr;
}

void ClientConnection::disconnect() {
    log("Disconnecting a connection...");
    connected = false;
    delete c;
}

bool ClientConnection::isConnected() { return connected; }

ClientConnection::ClientConnection() {
    connected = false;
    c = nullptr;
}

// Constructor + connect
ClientConnection::ClientConnection(Client* ptr) { connect(ptr); }

// Destructor + disconnect
ClientConnection::~ClientConnection() { disconnect(); }



int main(int argc, char *argv[])
{
    // Load configuration file
    std::vector<std::string> searchPathList;
    if (std::getenv("XDG_CONFIG_HOME") != NULL) {
        searchPathList.push_back((std::string) std::getenv("XDG_CONFIG_HOME") + "/termchat");
    }
    if (std::getenv("HOME") != NULL) {
        searchPathList.push_back((std::string) std::getenv("HOME") + "/.config/termchat");
        searchPathList.push_back((std::string) std::getenv("HOME") + "/.termchat");
    }
    searchPathList.push_back(".");

    for (std::string searchPath: searchPathList) {
        std::ifstream f_config(searchPath + "/server.json");
        if (f_config.is_open()) {
            f_config >> config;
            break;
        }
    }

    // Parse CLI options
    CLI::App app{"Official TermChat server"};

    // Set the port
    int port = 16310;
    if (config["listener"]["port"].is_null()) {
        port = config["listener"]["port"];
    }
    app.add_option("-p,--port", port, "TCP port to listen on. Default is 'config.listener.port' or '16310'.");

    // Set IP adress
    sf::IpAddress ip = "0.0.0.0";
    if (!config["listener"]["ip"].is_null()) {
        ip = (std::string) config["listener"]["ip"];
    }
    app.add_option("-i,--ip", ip, "IP adress to listen on. Default is 'config.listener.ip' or '0.0.0.0'.");

    CLI11_PARSE(app, argc, argv);

    log("Starting server on: ", ip, ":", port, "...");

    Server server(port, ip);

    // Listen for new connections
    log("Listening for connections...");
    signal(SIGINT, stopServer);

    server.listen();

    log("Stopping the server...");
}