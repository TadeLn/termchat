#define SOCKET_TIMEOUT sf::seconds(1.0f)

#include <iostream>
#include <string>
#include <fstream>

#include <cmath>
#include <cstdlib>

#include <SFML/Network.hpp>
#include <SFML/System.hpp>

#include <CLI/CLI.hpp>

#include <nlohmann/json.hpp>



using json = nlohmann::json;

class Client
{

public:
    int id;
    std::string nick;
    bool connected;

    sf::TcpSocket socket;
    std::vector<bool> *deleteThreads;

    Client();
    Client(Client&);
    Client(std::vector<bool>*);

};

Client::Client()
{
    id = 0;
    deleteThreads = nullptr;
    nick = "";
    connected = true;
}

Client::Client(Client &clientRef)
{
    id = clientRef.id;
    deleteThreads = clientRef.deleteThreads;
    nick = "";
    connected = true;
}

Client::Client(std::vector<bool> *deleteThreads)
{
    id = 0;
    this->deleteThreads = deleteThreads;
    nick = "";
    connected = true;
}



class ClientListEntry
{

public:
    int index;
    std::vector<Client*> *vectorPtr;
    ClientListEntry();
    ClientListEntry(std::vector<Client*>*);

};

ClientListEntry::ClientListEntry(std::vector<Client*> *vectorPtr) {
    this->vectorPtr = vectorPtr;
    index = 0;
}



enum MessageType {
    DATA_NULL,
    DATA_TEXT,
    DATA_ME,
    DATA_CHANGENICK,
    DATA_NICKDATA,
    DATA_CONNECTED,
    DATA_DISCONNECTED
};



json config;



std::stringstream makeSS() {
    std::stringstream ss;
    ss << '\n';
    return ss;
}

template<typename First, typename ... Rest>
std::stringstream makeSS(First first, Rest... rest) {
    std::stringstream ss;
    ss << first << makeSS(rest...).str();
    return ss;
}


template<typename ... Args>
void log(Args... args) {
    std::stringstream ss = makeSS(args...);

    bool logConsole = 1, logFile = 0;
    if (!config["logging"]["toConsole"].is_null()) {
        logConsole = config["logging"]["toConsole"];
    }
    if (!config["logging"]["toFile"].is_null()) {
        logFile = config["logging"]["toFile"];
    }

    if (logConsole) {
        std::cout << ss.str();
    }
    if (logFile) {
        std::string filename = "termchat.log";
        if (!config["logging"]["toFile"].is_null()) {
            filename = config["logging"]["file"];
        }
        
        std::ofstream file(filename, std::ios::app);
        file << ss.str();
    }
}

void sendToClients(std::vector<Client*> *vectorPtr, int clientId, char messageType, char *data, int dataSize)
{
    for (int i = 0; i < vectorPtr->size(); i++)
    {
        if (!vectorPtr->at(i)->connected) continue;
        //if (vectorPtr->at(i)->id == clientId) continue;

        char finalData[dataSize + 5];
        for (int j = 0; j < 4; j++)
        {
            finalData[j] = (int)(clientId / pow(256, j)) % 256;
        }
        finalData[4] = messageType;
        
        for (int j = 0; j < dataSize; j++)
        {
            finalData[j+5] = data[j];
        }

        if (vectorPtr->at(i)->socket.send(finalData, dataSize+5) != sf::Socket::Done)
        {
            log("Socket #", vectorPtr->at(i)->id, " Error!");
            continue;
        }
    }
}

void sendNicks(std::vector<Client*> *vectorPtr, int targetClientId)
{
    Client *client = vectorPtr->at(targetClientId);

    for (int i = 0; i < vectorPtr->size(); i++)
    {
        if (!vectorPtr->at(i)->connected) continue;
        if (vectorPtr->at(i)->id == targetClientId) continue;

        
        char finalData[vectorPtr->at(i)->nick.size() + 5];
        for (int j = 0; j < 4; j++)
        {
            finalData[j] = (int)(vectorPtr->at(i)->id / pow(256, j)) % 256;
        }
        finalData[4] = DATA_NICKDATA;
        
        for (int j = 0; j < vectorPtr->at(i)->nick.size(); j++)
        {
            finalData[j+5] = vectorPtr->at(i)->nick[j];
        }

        if (client->socket.send(finalData, vectorPtr->at(i)->nick.size()+5) != sf::Socket::Done)
        {
            log("Socket #", client->id, "Error!");
            continue;
        }
    }
}

void handleClientThread(ClientListEntry *clientListEntry)
{
    Client *client = clientListEntry->vectorPtr->at(clientListEntry->index);
    log("NEW CONNECTION #", client->id);
    char newData[] = {0};
    sendToClients(clientListEntry->vectorPtr, client->id, DATA_CONNECTED, newData, 1);

    sendNicks(clientListEntry->vectorPtr, client->id);
    client->nick = "";

    // Listen for packets
    bool stop = false;
    char data[1048576];
    std::size_t received;

    sf::SocketSelector selector;
    selector.add(client->socket);
    
    while (!stop)
    {
        if (selector.wait(SOCKET_TIMEOUT)) {
            // TCP socket
            if (client->socket.receive(data, 1048576, received) != sf::Socket::Done)
            {
                stop = true;
            }
            else
            {
                std::string text = "";
                char newData[received-1];
                switch (data[0])
                {
                    case DATA_TEXT:
                        for (int i = 1; i < received; i++) {
                            text += data[i];
                            newData[i-1] = data[i];
                        }
                        log("#", client->id, " (", client->nick, ") [", received, "]: ", text);

                        sendToClients(clientListEntry->vectorPtr, client->id, DATA_TEXT, newData, received-1);
                        break;

                    case DATA_ME:
                        for (int i = 1; i < received; i++) {
                            text += data[i];
                            newData[i-1] = data[i];
                        }
                        log("#", client->id, " (", client->nick, ") status [", received, "]: ", text);

                        sendToClients(clientListEntry->vectorPtr, client->id, DATA_ME, newData, received-1);
                        break;

                    case DATA_CHANGENICK:
                        for (int i = 1; i < received; i++) {
                            text += data[i];
                            newData[i-1] = data[i];
                        }
                        log("#", client->id, " (", client->nick, ") changed nick [", received, "]: ", text);
                        client->nick = text;

                        sendToClients(clientListEntry->vectorPtr, client->id, DATA_CHANGENICK, newData, received-1);
                        break;

                    default:
                        log("Unknown data type from #", client->id, " (", client->nick, ") [", received, "]: ", int(data[0]));
                        break;
                }
            }
        }
    }
    log("TERMINATED CONNECTION #", client->id);
    client->connected = false;
    newData[0] = {0};
    sendToClients(clientListEntry->vectorPtr, client->id, DATA_DISCONNECTED, newData, 1);
}



class DeleteOtherThreads_Data
{
    
public:
    std::vector<sf::Thread*> *threadsPtr;
    std::vector<bool> *deleteThreadsPtr;
    DeleteOtherThreads_Data();

};

DeleteOtherThreads_Data::DeleteOtherThreads_Data()
{
    threadsPtr = nullptr;
    deleteThreadsPtr = nullptr;
}

void deleteOtherThreads(DeleteOtherThreads_Data *data)
{
    while (true)
    {
        for (int i = 0; i < data->threadsPtr->size(); i++)
        {
            try
            {
                if (data->deleteThreadsPtr->at(i) == true)
                {
                    data->threadsPtr->at(i)->terminate();
                    delete data->threadsPtr->at(i);
                    data->threadsPtr = nullptr;
                    data->deleteThreadsPtr->at(i) = false;
                }
            }
            catch (std::exception e)
            {
                log(e.what());
            }
        }
        sf::sleep(sf::milliseconds(10));
    }
}



int main(int argc, char *argv[])
{
    // Load configuration file
    std::vector<std::string> searchPathList;
    if (std::getenv("XDG_CONFIG_HOME") != NULL) {
        searchPathList.push_back((std::string) std::getenv("XDG_CONFIG_HOME") + "/termchat");
    }
    if (std::getenv("HOME") != NULL) {
        searchPathList.push_back((std::string) std::getenv("HOME") + "/.config/termchat");
        searchPathList.push_back((std::string) std::getenv("HOME") + "/.termchat");
    }
    searchPathList.push_back(".");

    for (std::string searchPath: searchPathList) {
        std::ifstream f_config(searchPath + "/server.json");
        if (f_config.is_open()) {
            f_config >> config;
            break;
        }
    }

    // Parse CLI options
    CLI::App app{"Official TermChat server"};

    // Set the port
    int port = 16310;
    if (config["listener"]["port"].is_null()) {
        port = config["listener"]["port"];
    }
    app.add_option("-p,--port", port, "TCP port to listen on. Default is 'config.listener.port' or '16310'.");

    // Set IP adress
    sf::IpAddress ip = "0.0.0.0";
    if (!config["listener"]["ip"].is_null()) {
        ip = (std::string) config["listener"]["ip"];
    }
    app.add_option("-i,--ip", ip, "IP adress to listen on. Default is 'config.listener.ip' or '0.0.0.0'.");

    CLI11_PARSE(app, argc, argv);

    log("Starting server on: ", ip, ":", port, "...");

    log("Creating a Listener...");
    sf::TcpListener listener;

    // Bind the listener to a port
    if (listener.listen(port, ip) != sf::Socket::Done)
    {
        log("Listener Error!");
    }
    log("Creating a Listener... Done!");
    log("Listening for connections...");

    // Define all threads
    std::vector<sf::Thread*> threads;
    std::vector<bool> deleteThreads;

    // Clients
    std::vector<Client*>* clients = new std::vector<Client*>;

    // Start a thread that cleans up other threads
    DeleteOtherThreads_Data *data = new DeleteOtherThreads_Data;
    data->threadsPtr = &threads;
    data->deleteThreadsPtr = &deleteThreads;
    sf::Thread deleteOtherThreadsThread(&deleteOtherThreads, data);
    deleteOtherThreadsThread.launch();

    sf::SocketSelector selector;
    selector.add(listener);

    // Accept a new connections
    while (true) {
        if (selector.wait(SOCKET_TIMEOUT)) {
            Client *newClient = new Client(&deleteThreads);
            if (listener.accept(newClient->socket) != sf::Socket::Done)
            {
                log("Creating a new connection...");
                log("Connection Error!");
                continue;
            }
            log("Creating a new connection...");

            // Save client
            newClient->id = threads.size();
            clients->push_back(newClient);

            // Create a client list entry
            ClientListEntry *newClientListEntry = new ClientListEntry(clients);
            newClientListEntry->index = newClient->id;

            deleteThreads.push_back(false);
            threads.push_back(new sf::Thread(&handleClientThread, newClientListEntry));
            threads[threads.size()-1]->launch();
        }
    }
    log("Stopping the server...");
    deleteOtherThreadsThread.terminate();

    return 0;
}