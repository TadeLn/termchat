#define MAX_REFRESH_RATE 16
#define MAX_NICKNAME_LENGTH 32
#define MAX_PACKET_SIZE 1048576
#define COMMAND_PREFIX '/'
#define REMOVE_DOT 1
#define WORD_SEPARATORS {' '}
#define SOCKET_TIMEOUT sf::seconds(1.0f)

#include <string>
#include <thread>
#include <vector>
#include <sstream>
#include <chrono>
#include <mutex>

#include <clocale>

#include <SFML/Network.hpp>
#ifdef FEATURE_NOTIFICATION_SOUND
    #include <SFML/Audio.hpp>
#endif

#include <CLI/CLI.hpp>

#include <ncursesw/ncurses.h>

#ifdef FEATURE_NOTIFICATION_TOAST
    #ifdef __linux__
        #include <libnotify/notify.h>
    #endif
#endif



#ifdef FEATURE_NOTIFICATION_SOUND
    sf::SoundBuffer notifySoundBuffer;
    sf::Sound notificationSound;
#endif



std::stringstream makeSS() {
    return std::stringstream();
}

template<typename First, typename ... Rest>
std::stringstream makeSS(First first, Rest... rest) {
    std::stringstream ss;
    ss << first << makeSS(rest...).str();
    return ss;
}


template<typename ... Args>
std::string argsToStr(Args... args) {
    std::stringstream ss = makeSS(args...);
    return ss.str();
}

std::string joinVector(std::vector<std::string> vector) {
    std::stringstream ss;
    for (std::string element: vector) {
        ss << element;
    }
    return ss.str();
}

#ifdef FEATURE_NOTIFICATION_TOAST
    #ifdef __linux__
        void showNotification(std::string summary, std::string body) {
            notify_init("TermChat");
            NotifyNotification* n = notify_notification_new(summary.c_str(), body.c_str(), nullptr);
            notify_notification_set_timeout(n, 3000); // 5 seconds

            if (!notify_notification_show(n, 0)) 
            {
                std::cerr << "Couldn't show notification\n";
            }
        }
    #endif
#endif

enum DataType {
    DATA_NULL,
    DATA_TEXT,
    DATA_ME,
    DATA_CHANGENICK,
    DATA_NICK,
    DATA_CONNECTED,
    DATA_DISCONNECTED
};

enum EventType {
    EVENT_NULL,
    EVENT_DATA,
    EVENT_CLIENTMSG
};

class Event {

public:
    EventType type;

    std::string content;

    DataType data;
    int clientId;
    std::string clientNickname;

    std::string toString();
    void notify();

    #ifdef DEBUG_SHOW_PACKETS
        std::vector<unsigned char> rawdata;
    #endif
};

std::string Event::toString() {
    switch (type) {
        case EVENT_DATA:
            switch (data) {
                case DATA_NULL:
                    break;
                
                case DATA_TEXT:
                    return argsToStr(clientNickname, '#', clientId, ": ", content);
                
                case DATA_ME:
                    return argsToStr('*', clientNickname, '#', clientId, ' ', content);
                
                case DATA_CHANGENICK:
                    return argsToStr(clientNickname, '#', clientId, " has changed nick to ", content);
                
                case DATA_NICK:
                    return argsToStr(clientNickname, '#', clientId, " has nick ", content);
                
                case DATA_CONNECTED:
                    return argsToStr(clientNickname, '#', clientId, " has connected.");
                
                case DATA_DISCONNECTED:
                    return argsToStr(clientNickname, '#', clientId, " has disconnected.");
            }
            break;
        
        case EVENT_CLIENTMSG:
            break;
    }
    return "";
}

void Event::notify() {
    #ifdef FEATURE_NOTIFICATION_TOAST
        showNotification("TermChat", toString());
    #endif
    #ifdef FEATURE_NOTIFICATION_SOUND
        notificationSound.stop();
        notificationSound.play();
    #endif
    #ifdef FEATURE_NOTIFICATION_BEEP
        system("beep -f 392 -l 30 -nf 587 -l 70 -nf 523 -l 30 -nf 783 -l 70");
    #endif
}



class Client {

public:
    void connect(const std::string, const int);
    void disconnect();

    void sendMessage(const std::string);
    void sendStatus(const std::string);
    void changeNick(const std::string);

    Event getEvent();
    unsigned int getSelfId();
    std::vector<unsigned int> getUserList();
    std::string getUserNick(unsigned int);

private:
    static void listen(sf::TcpSocket&, std::vector<Event>&, bool&, std::vector<std::string>&, std::vector<bool>&, unsigned int&);
    void sendPacket(const char, const char*, const int);

    sf::TcpSocket socket;
    std::thread t_listen;
    std::vector<Event> events;
    unsigned int selfId;
    std::vector<std::string> userNicks;
    std::vector<bool> userConnected;
    bool stop;

};

void Client::connect(const std::string ipString, const int port) {
    selfId = -1;
    sf::IpAddress ip = ipString;

    sf::Socket::Status status = socket.connect(ip, port);
    if (status != sf::Socket::Done)
    {
        throw;
    }

    stop = 0;
    t_listen = std::thread(Client::listen, std::ref(socket), std::ref(events), std::ref(stop), std::ref(userNicks), std::ref(userConnected), std::ref(selfId));
}

void Client::disconnect() {
    stop = 1;
    t_listen.join();
}

void Client::sendMessage(const std::string message) {
    // Define data array and its length
    const int dataLength = message.length();
    char data[dataLength];
    
    // Copy the string to data array
    for (int i = 0; i < dataLength; i++) {
        data[i] = message[i];
    }

    // Send the packet
    sendPacket(DATA_TEXT, data, dataLength);
}

void Client::sendStatus(const std::string status) {
    // Define data array and its length
    const int dataLength = status.length();
    char data[dataLength];
    
    // Copy the string to data array
    for (int i = 0; i < dataLength; i++) {
        data[i] = status[i];
    }

    // Send the packet
    sendPacket(DATA_ME, data, dataLength);
}

void Client::changeNick(const std::string nick) {
    if (nick.length() <= MAX_NICKNAME_LENGTH) {
        // Define data array and its length
        const int dataLength = nick.length();
        char data[dataLength];

        // Copy the string to data array
        for (int i = 0; i < dataLength; i++) {
            data[i] = nick[i];
        }

        // Send the packet
        sendPacket(DATA_CHANGENICK, data, dataLength);
    } else {
        throw;
    }
}

Event Client::getEvent() {
    Event event;
    if (!events.empty()) {
        event = events[0];
        events.erase(events.begin());
    } else {
        event.type = EVENT_NULL;
    }
    return event;
}

unsigned int Client::getSelfId() {
    return selfId;
}

std::vector<unsigned int> Client::getUserList() {
    std::vector<unsigned int> result;
    for (unsigned int i = 0; i < userConnected.size(); i++) {
        if (userConnected[i]) {
            result.push_back(i);
        }
    }
    return result;
}

std::string Client::getUserNick(unsigned int id) {
    return userNicks[id];
}

void Client::listen(sf::TcpSocket &socket, std::vector<Event> &events, bool &stop, std::vector<std::string> &userNicks, std::vector<bool> &userConnected, unsigned int &selfId) {
    char receivedData[MAX_PACKET_SIZE];
    std::size_t receivedSize;

    sf::SocketSelector selector;
    selector.add(socket);

    while (!stop) {
        if (selector.wait(SOCKET_TIMEOUT)) {
            if (socket.receive(receivedData, 1048576, receivedSize) == sf::Socket::Done) {
                int clientId = (receivedData[3] * 16777216) + (receivedData[2] * 65536) + (receivedData[1] * 256) + receivedData[0];
                while (userNicks.size() <= clientId) {
                    userNicks.push_back("");
                }

                while (userConnected.size() <= clientId) {
                    userConnected.push_back(false);
                }

                std::string text;
                Event event;
                event.type = EVENT_DATA;
                
                #ifdef DEBUG_SHOW_PACKETS
                    for (unsigned int i = 0; i < receivedSize; i++) {
                        event.rawdata.push_back((unsigned char)receivedData[i]);
                    }
                #endif

                switch (receivedData[4]) {
                    case DATA_TEXT:
                        for (int i = 5; i < receivedSize; i++)
                        {
                            text += receivedData[i];
                        }
                        event.data = DATA_TEXT;
                        event.clientId = clientId;
                        event.clientNickname = userNicks[clientId];
                        event.content = text;
                        userConnected[clientId] = true;
                        break;

                    case DATA_ME:
                        for (int i = 5; i < receivedSize; i++)
                        {
                            text += receivedData[i];
                        }
                        event.data = DATA_ME;
                        event.clientId = clientId;
                        event.clientNickname = userNicks[clientId];
                        event.content = text;
                        userConnected[clientId] = true;
                        break;

                    case DATA_CHANGENICK:
                        for (int i = 5; i < receivedSize; i++)
                        {
                            text += receivedData[i];
                        }
                        event.data = DATA_CHANGENICK;
                        event.clientId = clientId;
                        event.clientNickname = userNicks[clientId];
                        event.content = text;
                        userNicks[clientId] = text;
                        userConnected[clientId] = true;
                        break;

                    case DATA_NICK:
                        event.data = DATA_NICK;
                        event.clientId = clientId;
                        event.clientNickname = userNicks[clientId];
                        event.content = text;
                        userNicks[clientId] = text;
                        userConnected[clientId] = true;
                        break;

                    case DATA_CONNECTED:
                        event.data = DATA_CONNECTED;
                        event.clientId = clientId;
                        event.clientNickname = userNicks[clientId];
                        if (selfId == -1) {
                            selfId = clientId;
                        }
                        userConnected[clientId] = true;
                        break;

                    case DATA_DISCONNECTED:
                        event.data = DATA_DISCONNECTED;
                        event.clientId = clientId;
                        event.clientNickname = userNicks[clientId];
                        userConnected[clientId] = false;
                        break;

                    default:
                        break;
                }
                if (clientId != selfId) {
                    event.notify();
                }
                events.push_back(event);
            } else {
                stop = 1;
            }
        }
    }
    socket.disconnect();
}

void Client::sendPacket(const char messageType, const char *data, const int dataLength)
{
    char finalData[dataLength+1];
    finalData[0] = messageType;
    for (int i = 0; i < dataLength; i++) {
        finalData[i+1] = data[i];
    }
    if (socket.send(finalData, dataLength+1) != sf::Socket::Done) {
        throw;
    }
}

void handleInput(std::string input, Client &client, bool &stop, WINDOW *win) {
    if (!input.empty()) {
        if (input[0] == COMMAND_PREFIX) {
            input.erase(input.begin());
            std::vector<std::string> args;
            std::string tmp;
            for (char ch: input) {
                if (ch == ' ') {
                    args.push_back(tmp);
                    tmp = "";
                } else {
                    tmp += ch;
                }
            }
            args.push_back(tmp);

            std::string command = args[0];
            args.erase(args.begin());

            if (command == "me") {
                client.sendStatus(joinVector(args));
            } else if (command == "nick") {
                client.changeNick(args[0]);
            } else if (command == "exit") {
                stop = 1;
            } else if (command == "list") {
                std::string result = "";
                std::vector<unsigned int> userIds = client.getUserList();

                for (unsigned int i = 0; i < userIds.size(); i++) {
                    result += client.getUserNick(userIds[i]) + "#" + std::to_string(userIds[i]);
                    if (i != userIds.size()) {
                        result += " ";
                    }
                }

                if (win == nullptr) {
                    std::cout << "* Connected users: \n" << result << "\n";
                } else {
                    wprintw(win, "* Connected users: %s\n", result.c_str());
                    wrefresh(win);
                }
            } else if (command == "id") {
                if (win == nullptr) {
                    std::cout << "* Your id: " << client.getSelfId() << "\n";
                } else {
                    wprintw(win, "* Your id: %d\n", client.getSelfId());
                    wrefresh(win);
                }
            } else {
                throw;
            }
        } else {
            if (input[0] == '.' && REMOVE_DOT) {
                input.erase(input.begin());
            }
            client.sendMessage(input);
        }
    }
}

namespace plaintextUI {

void printEvents(Client &client, bool &stop) {
    while (!stop) {
        std::this_thread::sleep_for(std::chrono::milliseconds(MAX_REFRESH_RATE));
        Event event = client.getEvent();
        if (event.type != EVENT_NULL) {
            #ifdef DEBUG_SHOW_PACKETS
                for (unsigned int i = 0; i < event.rawdata.size(); i++) {
                    std::cout << event.rawdata[i];
                }
                std::cout << "\n";
            #endif
            std::cout << event.toString() << "\n";
        }
    }
}

void start(int port, std::string ip) {
    if (ip.empty()) {
        std::cout << "IP: ";
        getline(std::cin, ip);
    }

    Client client;

    client.connect(ip, port);

    bool stop = 0;
    std::thread t_printEvents(printEvents, std::ref(client), std::ref(stop));

    std::string input;
    while (!stop) {
        getline(std::cin, input);
        handleInput(input, client, stop, nullptr);
    }

    client.disconnect();
    t_printEvents.join();
}

}

namespace ncursesUI {

int unicodeLength(std::string string, int end = -1) {
    if (end == -1) {
        end = string.length();
    }
    
    int count = 0;
    for (int i = 0; i < end; ++i) {
        unsigned char byte = string[i];
        if (byte >> 6 == 0b11 || byte >> 7 == 0b0) {
            ++count;
        }
    }

    return count;
}

int unicodeCountBytes(std::string string, int start = 0, int chars = 1, bool backwards = 0) {
    int byteNum = 0;
    int chNum = 0;
    int i = start;
    while (true) {
        unsigned char byte = string[i];
        if (byte >> 6 == 0b11 || byte >> 7 == 0b0) {
            ++chNum;
        }
        if (chNum > chars) {
            return byteNum;
        }
        ++byteNum;
        if (!backwards) {
            ++i;
        } else {
            --i;
        }
    }
}

void printEvents(Client &client, bool &stop, WINDOW *win, std::mutex &consoleWrite) {
    while (!stop) {
        Event event = client.getEvent();
        if (event.type != EVENT_NULL) {
            consoleWrite.lock();
            wprintw(win, "%s\n", event.toString().c_str());
            #ifdef DEBUG_SHOW_PACKETS
                for (unsigned int i = 0; i < event.rawdata.size(); i++) {
                    wprintw(win, "%d ", (int)event.rawdata[i]);
                }
                wprintw(win, "\n");
            #endif
            wrefresh(win);
            consoleWrite.unlock();
        } else {
            std::this_thread::sleep_for(std::chrono::milliseconds(MAX_REFRESH_RATE));
        }
    }
}

void start(int port, std::string ip) {
    // Ask for IP if it's not set
    if (ip.empty()) {
        std::cout << "IP: ";
        getline(std::cin, ip);
    }

    // Define the client
    Client client;

    client.connect(ip, port);

    initscr();
    noecho();

    int maxX, maxY;
    getmaxyx(stdscr, maxY, maxX);

    WINDOW* sendWin = newwin(1, maxX + 1, maxY - 1, 0);
    keypad(sendWin, 1);

    WINDOW* consoleWin = newwin(maxY, maxX + 1, 0, 0);
    scrollok(consoleWin, 1);

    std::mutex consoleWrite;

    bool stop = 0;
    std::thread t_printEvents(printEvents, std::ref(client), std::ref(stop), consoleWin, std::ref(consoleWrite));

    unsigned int scroll = 0;

    std::string input;
    int cursorPos = 0;
    std::vector<char> wordSeparators = WORD_SEPARATORS;
    std::string prompt = "> ";

    while (!stop) {
        consoleWrite.lock();

        wclear(sendWin); // Clear window
        wprintw(sendWin, prompt.c_str()); // Print the prompt
        wprintw(sendWin, input.c_str()); // Print already inputted text
        wmove(sendWin, 0, unicodeLength(input, cursorPos) + prompt.length()); // Move cursor
        wrefresh(sendWin); // Refresh window

        consoleWrite.unlock(); // Unlock writing

        int ch = wgetch(sendWin); // Get pressed character
        switch (ch) {
            case 343:
                // Insert new line
                input.insert(cursorPos, "\n");
                ++cursorPos;
                break;

            case '\n': // New line / Enter
                // Confirm the input
                handleInput(input, client, stop, consoleWin);
                input = "";
                cursorPos = 0;
                break;
            
            case KEY_BACKSPACE:
            case 8: // Backspace
                if (!input.empty() && cursorPos > 0) {
                    // Get the size of the char before the cursor
                    unsigned char charSize = unicodeCountBytes(input, cursorPos, 1, true);

                    // Erase this character
                    input.erase(cursorPos - charSize, charSize);

                    // Move the cursor
                    cursorPos -= charSize;
                }
                break;
            
            case 330: // Delete
                if (!input.empty() && cursorPos < input.length()) {
                    // Delete the character that the cursor is on
                    input.erase(cursorPos, unicodeCountBytes(input, cursorPos, 1));
                }
                break;
            
            case KEY_LEFT:
                // Move cursor backwards 1 character
                cursorPos -= unicodeCountBytes(input, cursorPos, 1, true);
                break;
            
            case KEY_RIGHT:
                // Move cursor forwards 1 character
                cursorPos += unicodeCountBytes(input, cursorPos, 1);
                break;
            
            case KEY_UP:
                // Scroll messages upwards
                scroll++;
                break;
            
            case KEY_DOWN:
                // Scroll messages downwards
                if (scroll > 0) {
                    scroll--;
                }
                break;
            
            case KEY_HOME:
                // Set cursor position to the first character
                cursorPos = 0;
                break;
            
            case KEY_END:
                // Set cursor position to an empty space after the string
                cursorPos = input.length();
                break;

            case 410:
                // Do nothing
                break;

            case 545: // Ctrl+Left
            case 546:
                // Move cursor backwards 1 word
                do {
                    cursorPos -= unicodeCountBytes(input, cursorPos, 1, true);
                    // Check if cursor is at the beggining of the string
                    if (cursorPos <= 0) {
                        // If yes, stop
                        cursorPos = 0;
                        break;
                    }
                    // Stop if there is a word separator on the cursor
                } while (find(wordSeparators.begin(), wordSeparators.end(), input[cursorPos]) == wordSeparators.end());
                break;
            
            case 560: // Ctrl+Right
            case 561:
                // Move cursor forwards 1 word
                do {
                    // Check if cursor is at the end of the string
                    cursorPos += unicodeCountBytes(input, cursorPos, 1);
                    if (cursorPos >= input.length()) {
                        // If yes, stop
                        cursorPos = input.length();
                        break;
                    }
                    // Stop if there is a word separator on the cursor
                } while (find(wordSeparators.begin(), wordSeparators.end(), input[cursorPos]) == wordSeparators.end());
                break;

            default: // Any other key
                // Insert char to "input" string
                input.insert(cursorPos, std::string(1, ch));
                ++cursorPos;
                break;
        }

        // Check if cursor is in bounds
        if (cursorPos < 0) {
            cursorPos = 0;
        } else if (cursorPos > input.length()) {
            cursorPos = input.length();
        }
    }

    // Disconnect from the server
    client.disconnect();

    // Wait for printEvents thread to finish
    t_printEvents.join();

    // Close the window
    endwin();
}

}

int main(int argc, char *argv[]) {
    setlocale(LC_ALL, "");
    
    // Define the app
    CLI::App app{"Official TermChat client"};

    // Define argv options
    int port = 16310;
    app.add_option("-p,--port", port, "Port to connnect to. Default is 16310.");

    std::string ip;
    app.add_option("-i,--ip", ip, "IP adress to connnect to.");

    // Get the port and the ip from argv
    CLI11_PARSE(app, argc, argv);

    #ifdef FEATURE_NOTIFICATION_SOUND
        // Try to load sound
        std::vector<std::string> filenames = {"client/notify.wav", "notify.wav"};
        notificationSound.setVolume(0);
        for (unsigned int i = 0; i < filenames.size(); i++) {
            if (notifySoundBuffer.loadFromFile(filenames[i])) {
                std::cout << "Succesfully loaded notification sound " << filenames[i] << "\n";
                notificationSound.setBuffer(notifySoundBuffer);
                notificationSound.setVolume(100);
                break;
            }
        }
    #endif

    // Start the application
    ncursesUI::start(port, ip);
}